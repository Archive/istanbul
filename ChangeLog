2008-10-17  Stefan Kost  <ensonic@users.sf.net>

	* configure.ac:
	* gst/Makefile.am:
	  Actualy define GST_PLUGIN_LDFLAGS (avoid installing plugin as
	  versioned library). Also add libtool magic to avoid installing static
	  library.

2008-04-23  Luca Bruno  <lucab@debian.org>

	patch by: Krzysztof Kotlenga

	* istanbul/main/window_select.py
	Fix window selection under compiz and other composite
	enabled window manager.
	Closes #412663

2008-04-15  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	patch by: Florian Boucault

	* istanbul/main/screencast.py:
	Workaround issue with oggmux, note this is not a fix.
	Related to bug #430151

2008-04-15  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	patch by: Christophe Dehais

	* istanbul/main/area_select.py:
	Return xoordinates in order in get_area().
	Fixes bug #484866

2007-08-12  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	Patches from: Florian Boucault, Chris Rivas, Luca Bruno

	* istanbul/main/save_window.py:
	Fix saving to disk to respect umask setting.
	Fixes bug #429289

2007-04-30  Zaheer Abbas Merali  <<zaheerabbas at merali dot org>>

	* istanbul/main/screencast.py (CmdlineScreencast.start_recording):
	Fix recording from command line when not specifying coordinates.
	Thanks to Chris for spotting.
	Fixes bug #434389

2007-04-27  Zaheer Abbas Merali  <<zaheerabbas at merali dot org>>

	* gst/gstximagesrc.c (gst_istximage_src_ximage_get):
	Fix leak of buffers.

2007-04-27  Zaheer Abbas Merali  <<zaheerabbas at merali dot org>>

	* gst/gstximagesrc.c (gst_istximage_src_start,
	  gst_istximage_src_ximage_get):
	* gst/gstximagesrc.h (last_ximage):
	Fix really stupid bug where we never copy the previous frame
	to the buffer when using damage.

2007-04-27  Zaheer Abbas Merali  <<zaheerabbas at merali dot org>>

	* istanbul/main/preferences.py (Preferences.__init__):
	Add preference for recording in images.
	* istanbul/main/screencast.py (Screencast.start_recording,
	  Screencast.stop_recording, Screencast.on_eos):
	Calculate widths and heights correctly!
	Add start of code that does still image recording.
	* istanbul/main/tray_icon.py (TrayIcon.stop_handler):
	Do not throw exception if someone clicked stop almost straight
	after start.

2007-04-18  Zaheer Abbas Merali  <<zaheerabbas at merali dot org>>

	* istanbul/main/main.py (main):
	Improve error message when we cannot find istximagesrc.

2007-04-18  Zaheer Abbas Merali  <<zaheerabbas at merali dot org>>

	* istanbul/main/screencast.py (Screencast.start_recording):
	Improve quality of output.

2007-04-16  Zaheer Abbas Merali  <<zaheerabbas at merali dot org>>

	patch by: Luca Bruno

	* istanbul/main/screencast.py (Screencast.start_recording):
	Fix recording area calculation.

2007-04-16  Zaheer Abbas Merali  <<zaheerabbas at merali dot org>>

	patch by: Florian Boucault

	* bin/istanbul.in:
	Initialise GStreamer after setting GST_PLUGIN_PATH.
	Fixes #430112.

2007-02-23  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* configure.ac:
	Back to svn.

2007-02-23  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* NEWS:
	* configure.ac:
	Release of 0.2.2

2007-02-23  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* gst/gstximagesrc.c: (composite_pixel):
	Backport fix from gst-plugins-good.

2007-02-23  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	Patch from: Marc-Andre Lureau

	* configure.ac:
	Check for more python modules.  Fixes bug #365810.

2007-02-22  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	Patch from: Steve Frecinaux

	* bin/istanbul.in:
	Fix GST_PLUGIN_PATH usage, fixes bug #40878.

2006-11-17  Wouter Bolsterlee  <wbolster@cvs.gnome.org>

	* istanbul/main/save_window.py:
	Close the save dialog after usage. Did some refactoring
	of the save method as well. Fixes bug #365919.

2006-10-28  Wouter Bolsterlee  <wbolster@cvs.gnome.org>

	* istanbul/main/main.py:
	Import pygtk for correctness.

	* istanbul/main/save_window.py:
	Mark missing string for translation.

	* istanbul/main/tray_icon.py:
	Use gtk.STOCK_MEDIA_RECORD as the default window icon.

2006-10-27  Wouter Bolsterlee  <wbolster@cvs.gnome.org>

	* istanbul/main/area_select.py:
	* istanbul/main/command_line.py:
	* istanbul/main/constants.py:
	* istanbul/main/preferences.py:
	* istanbul/main/tray_popup.py:
	* istanbul/main/window_select.py:

	Don't mix tabs and spaces in Python code.

2006-10-27  Wouter Bolsterlee  <wbolster@cvs.gnome.org>

	* istanbul/main/save_window.py:

	Use better wording for save dialog and improve button
	handling so that closing the "are you sure?" dialog
	handles the "closed by pressing X" event correctly.

2006-10-27  Wouter Bolsterlee  <wbolster@cvs.gnome.org>

	* istanbul/main/save_window.py:
	* istanbul/main/screencast.py:

	Fix focus issue with default button in the save dialog.
	Part of a fix for bug #355158.

	Removed (commented out, actually) the "Edit" button in
	save dialog, since it seems to do nothing at all. Fixes
	bug #365883.

	Fixed some tabs/spaces issues as well.

2006-10-27  Wouter Bolsterlee  <wbolster@cvs.gnome.org>

	* .cvsignore:
	* bin/.cvsignore:
	* common/.cvsignore:
	* data/.cvsignore:
	* doc/.cvsignore:
	* doc/man/.cvsignore:
	* gst/.cvsignore:
	* istanbul/.cvsignore:
	* istanbul/configure/.cvsignore:
	* istanbul/main/.cvsignore:
	* po/.cvsignore:
	Updated ignore patterns.

2006-10-27  Wouter Bolsterlee  <wbolster@cvs.gnome.org>

	* istanbul/main/save_window.py:
	Use better button labels. Fixes bug #353193.
	(I also fixed some tabs vs. spaces problems)

2006-10-27  Wouter Bolsterlee  <wbolster@cvs.gnome.org>

	* istanbul/configure/config.py.in:
	* istanbul/main/main.py:
	Moved i18n initialization to the config file, so that it
	loads early. This makes the interface appear in the
	correct language. Fixes bug #365805.

2006-10-27  Wouter Bolsterlee  <wbolster@cvs.gnome.org>

	* istanbul/main/tray_popup.py: Add destroy callback to
	the about dialog. This is needed for gtk 2.10 and up.

2006-10-27  Wouter Bolsterlee  <wbolster@cvs.gnome.org>

	* istanbul/main/main.py: Typo fixes in strings.
	* istanbul/main/tray_popup.py: Use 'translator-credits'
	string in the about dialog instead of an empty string
	(the empty string is replaced by the PO-header, which is
	not what should be displayed).

2006-08-27  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/Makefile.am:
	* istanbul/main/area_select.py:
	Rename select to area_select.
	* istanbul/main/window_select.py:
	* istanbul/main/main.py:
	* istanbul/main/preferences.py:
	* istanbul/main/screencast.py:
	* istanbul/main/tray_popup.py:
	Add Select Window support.  Patch from: Chong Kai Xiong.
	Fixes bug #349160.
	* po/POTFILES.in:
	Add area_select and window_select for translations.

2006-08-11  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/select.py:
	Make selector green when area is big enough, red when not.
	Also show a dialog when user has selected too small an area.
	Fixes bug #349974.

2006-08-04  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/select.py:
	Fix swapping code.

2006-08-04  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/select.py:
	Use outline not filled rectangle.  Fix when bottom right
	corner selected first.
	
2006-08-04  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* gst/ximageutil.c: (istximageutil_xcontext_get):
	Fix rgb masks on < 24bpp. Fixes bug #349684.

2006-08-01  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/save_window.py:
	Add Ogg Filter to save window's file chooser widget.
	Fixes bug #349588.

2006-08-01  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/gst_player.py:
	Always use ximagesink regardless of what gconf says.
	Fixes bug #347761.

2006-08-01  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/screencast.py:
	Pass display-name and screen-number to the GStreamer
	element and not rely on defaults.  Fixes bug #349561.

2006-08-01  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* configure.ac:
	In configure check for gst-python, import gobject first
	before pygst.  Fixes bug #349479.

2006-08-01  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/command_line.py:
	* istanbul/main/main.py:
	* istanbul/main/screencast.py:
	`istanbul --record FILE` will immediately start a screencast,
	recording to FILE, and won't add a tray icon.
	The screencast can be stopped by SIGTERM, istanbul will then stop
	recording, wait for end of stream and quit.
	
	Patch from: Frederic Peters
	Fixes bug #349177.

2006-07-29  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/tray_icon.py:
	* istanbul/main/tray_popup.py:
	Add tooltips.  Fixes bug #349213.

2006-07-28  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* configure.ac:
	Back to CVS.

2006-07-28  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* NEWS:
	Release of 0.2.1
	* configure.ac:
	Release of 0.2.1
	* istanbul/main/gst_player.py:
	Revert fix for resize segfault.
	* istanbul/main/screencast.py:
	Add translatable string.

2006-07-28  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/gst_player.py:
	Fix segfault when resizing save window before clicking play.

2006-07-28  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/gst_player.py:
	Better error reporting on playback of screencast.  
	Fixes #348320.

2006-07-28  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/screencast.py:
	Add videorate, should solve sound issues and possible issues.

2006-07-25  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/screencast.py:
	Make recording without sound work.  Patch from: 
	Marc-Andre Lureu.  Fixes #348540.

2006-07-23  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/screencast.py:
	Stop live recording properly, not set pipeline to NULL.
	Fixes bug #348340.

2006-07-22  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/screencast.py:
	* istanbul/main/tray_icon.py:
	Display errors if they occur on the Gst Bus.

2006-07-21  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/Makefile.am:
	Add checkradio widget to Makefile

2006-07-21  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* bin/istanbul.in:
	Set GST_PLUGIN_PATH so it has where we installed the plugin.
	Fix bug #348223

2006-07-21  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/preferences.py:
	* istanbul/main/screencast.py:
	* istanbul/main/tray_popup.py:
	Add sound recording support.

2006-07-21  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/tray_icon.py:
	Remove debug print statement.

2006-07-21  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/save_window.py:
	Ignore exception, when duration of saved file is 0.
	* istanbul/main/screencast.py:
	* istanbul/main/tray_icon.py:
	Show hard disk icon when save window is still around.

2006-07-21  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/Makefile.am:
	* istanbul/main/afterrecord.py:
	* istanbul/main/checkradio_widget.py:
	* istanbul/main/command_line.py:
	* istanbul/main/constants.py:
	* istanbul/main/gconf_client.py:
	* istanbul/main/gst_player.py:
	* istanbul/main/main.py:
	* istanbul/main/preferences.py:
	* istanbul/main/save_window.py:
	* istanbul/main/screen.py:
	* istanbul/main/screencast.py:
	* istanbul/main/tray_icon.py:
	* istanbul/main/tray_popup.py:

	Patch from: John N. Laliberte
	Refactor of Istanbul.  Fixes bug #347523.
	Current regression: editing video_size in gconf will not update gui.

2006-07-21  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* configure.ac:
	* po/LINGUAS:
	
	Patch from: Remi Cardona, fixes bug #347795.
	Also set release version to cvs mode (4th digit being 1)

2006-07-18  Guntupalli Karunakar  <karunakar@indlinux.org>

	* configure.ac: Added 'hi' to ALL_LINGUAS.

2006-07-14  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* NEWS:
	Release notes for 0.2.0.

2006-07-14  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* gst/gstximagesrc.c: (gst_istximage_src_ximage_get):
	Fix cursor display when only garbbing part of a screen.

2006-07-14  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/main.py:
	Fix typo that made recording mousepointer not work.

2006-07-14  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* configure.ac:
	* data/Makefile.am:
	Fix build of gconf schemas.  Prepare for release.

2006-07-14  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* gst/gstximagesrc.c: (gst_istximage_src_ximage_get):
	Fix segfault.

2006-07-14  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* configure.ac:
	* data/Makefile.am:
	Added stuff so gconf schema gets installed.

2006-07-14  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* data/Makefile.am:
	* data/istanbul.schemas.in:
	Created gconf schema.

2006-07-13  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* common/gst-feature.m4:
	Need this file.
	* configure.ac:
	Remove the conditional, not sure if that was a good idea.

2006-07-12  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/main.py:
	Check for plugin istximagesrc not istanbul.

2006-07-12  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* Makefile.am:
	Add gst dir, which contains our copy of ximagesrc.
	* common/Makefile.am:
	Add gst-feature.
	* configure.ac:
	Fix egg.trayicon test so it does not need X.  Add stuff needed for our
	local copy of ximagesrc.
	* istanbul/main/main.py:
	Replace ximagesrc with istximagesrc, our local copy.
	* gst/Makefile.am:
	* gst/gstximagesrc.c:
	* gst/gstximagesrc.h:
	* gst/ximageutil.c:
	* gst/ximageutil.h:
	Add local ximagesrc plugin.

2006-07-12  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/main.py:
	Fix typo.
	* po/POTFILES.in:
	Remove prefs.glade and eggtrayicon.py

2006-07-10  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/main.py:
	Fix bogus framerates input.

2006-07-09  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* configure.ac:
	* istanbul/main/main.py:
	Remove support for GStreamer 0.8.  Add check for egg.trayicon in
	gnome-python-extras.

2006-07-09  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/afterrecord.py:
	Fix behaviour when cancelling save dialog after saving.  Allow playing of
	file after saving in video widget.
	* istanbul/main/main.py:
	Add option to record mouse pointer.

2006-07-09  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/main.py:
	Add 3d recording option, which turns off use of XDamage in ximagesrc.
	Add mnemonics to popup menu items.

2006-07-09  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* README:
	Add list of dependencies.
	* configure.ac:
	* data/Makefile.am:
	* data/prefs.glade:
	Remove glade preferences file.
	* istanbul/Makefile.am:
	* istanbul/extern/Makefile.am:
	* istanbul/extern/__init__.py:
	Remove in-house pytrayicon, depend on gnome-python-extras.  Also fix silly
	framerate bug.  Both patches from Luca Bruno <kaeso at email dot it>.
	* istanbul/main/Makefile.am:
	* istanbul/main/afterrecord.py:
	Add code, to do the filename save after stopping recording.
	* istanbul/main/main.py:
	Add code to use gconf.  Thanks to Kiddo <nekohayo at gmail dot com> for
	the initial patch.
	* istanbul/main/prefs.py:
	Remove preferences code.
	* istanbul/main/select.py:
	Refactor byzanz's selector code into a python widget.  Thanks Benjamin
	Otte for writing a cool selector widget.

2006-07-07  Christian Rose  <menthos@menthos.com>

	* configure.ac: Added "sv" to ALL_LINGUAS.

2006-06-29  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	Patch from: Luca Bruno

	* Makefile.am:
	* configure.ac:
	* doc/Makefile.am:
	* doc/man/Makefile.am:
	* doc/man/istanbul.1:

	Fixes bug #345728: Manpage for istanbul.

2006-06-24  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	Patch from: Luca Bruno

	* istanbul/main/main.py:
	Fixes bug #345727: Popup menu not displayed if at bottom panel.

2006-06-22  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/main.py:
	Add videorate to make sure videos produced don't play too fast.

2006-06-17  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* NEWS:
	Release notes for 0.10.2.

2006-06-17  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/prefs.py:
	Make encode_later toggle unsensitive if on 0.10 because this does not
	work on 0.10 gstreamer.

2006-06-17  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* Makefile.am:
	Add necessary intltool files to dist.

2006-05-29  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* configure.ac:
	Bump version number.
	* istanbul/main/main.py:
	Check for elements beforehand.

2006-03-06  Edward Hervey  <edward@fluendo.com>

	* istanbul/main/main.py:
	Make it work (tm)
	allow running main uninstalled
	* istanbul/main/prefs.py:
	Fix for gstreamer 0.10 behaviour with element_factory_make raising
	an exception.

2006-03-06  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* common/as-python.m4:
	* configure.ac:
	* istanbul/main/main.py:
	Initial port to 0.10, requires gst-plugins-bad cvs currently for
	ximagesrc and encode later doesnt work in 0.10 version

2006-01-29  Alessio Frusciante  <algol@firenze.linux.it>

	* configure.ac: Added "it" (Italian) to ALL_LINGUAS.

2005-08-08  Danilo Šegan  <danilo@gnome.org>

	Prepare for internationalisation.

	* po/, po/POTFILES.in: Added.
	
	* data/Makefile.am: Install i18n-ed .desktop file.
	* data/istanbul.desktop: Move to istanbul.desktop.in and mark for translation.
	
	* istanbul/main/main.py (Istanbul._about): Mark strings for translation.
	(main): Call setlocale, bindtextdomain and textdomain.

	* istanbul/main/prefs.py (IstanbulPrefs.__init__): Add call to
	gtk.glade.bindtextdomain, add "domain" parameter to gtk.glade.XML. 

	* configure.ac (AC_OUTPUT): Added po/Makefile.in.
	Added stuff to set up translation.

	* istanbul/configure/config.py.in: Added 'localedir', 'domain'.

2005-07-04  Michael Scherer  <misc at mandriva dot org>

	reviewed by: Zaheer Abbas Merali

	* istanbul/main/main.py:
	* istanbul/main/prefs.py:
	fix icecast2 streaming and fix a bug when not saving

2005-07-01  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/main.py:
	For encode later, use a slower encoding path with theora to get a better picture

2005-07-01  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* istanbul/main/prefs.py:
	Remove print

2005-07-01  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* configure.ac:
	* istanbul/main/main.py:
	Preparing for release of 0.1.1 and change of web page in about dialog

2005-07-01  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* bin/istanbul.in:
	* configure.ac:
	Create and use a PYTHONDIR so executable knows where module is located
	if not in default prefix
	* data/prefs.glade:
	Add an encode later toggle
	* istanbul/configure/Makefile.am:
	use $(pythondir) coz j5 forgot to fix this Makefile.am
	* istanbul/main/main.py:
	* istanbul/main/prefs.py:
	Support encoding later, using an intermediary encoding of smoke

2005-06-30  John (J5) Palmieri  <johnp@redhat.com>

	* acinclude.m4: New file includes m4 scripts in ./common
	* istanbul/Makefile.am, istanbul/main/Makefile.am
	istanbul/extern/pytrayicon/Makefile.am, istanbul/extern/Makefile.am: 
	use $(pythondir) to determine where to install python modules

2005-06-13  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* data/prefs.glade:
	* istanbul/main/prefs.py:
	More UI gloss

2005-06-13  Zaheer Abbas Merali <zaheerabbas at merali dot org>

	* istanbul/configure/config.py.in:
	Fix config.py

2005-06-13  Zaheer Abbas merali  <zaheerabbas at merali dot org>

	* data/Makefile.am:
	* data/prefs.glade:
	Pretty up the preferences UI a bit

2005-06-12  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	Adapted patch from: Edward Hervey <bilboed at gmail dot com>

	* data/prefs.glade:
	* istanbul/main/main.py:
	Clean up some UI (padding wise)
	Add framerate setting

2005-06-12  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	Adapted patch from: j at v2v dotcc

	* data/Makefile.am:
	* data/prefs.glade:
	* istanbul/main/main.py:
	* istanbul/main/prefs.py:
	Use glade-based preferences dialog
	Offer save to disk as well as stream to icecast server

2005-06-12  Zaheer Abbas Merali  <zaheerabbas at merali dot org>

	* configure.ac:
	* istanbul/Makefile.am:
	* istanbul/configure/Makefile.am:
	* istanbul/configure/__init__.py:
	* istanbul/configure/config.py.in:
	* istanbul/main/main.py:
	Add configure variables accessible from python, 
	borrowed structure from flumotion.
	Made about dialog use version


07 Jun 2005; Zaheer Abbas Merali <zaheerabbas at merali dot org> 
Initial release 0.1.0

