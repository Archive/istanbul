# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4
#
# Istanbul - A desktop recorder
# Copyright (C) 2005 Zaheer Abbas Merali (zaheerabbas at merali dot org)
# Copyright (C) 2006 John N. Laliberte (allanonjl@gentoo.org) (jlaliberte@gmail.com)
# Portions Copyright (C) 2004,2005 Fluendo, S.L. (www.fluendo.com).
# All rights reserved.

# This file may be distributed and/or modified under the terms of
# the GNU General Public License version 2 as published by
# the Free Software Foundation.
# This file is distributed without any warranty; without even the implied
# warranty of merchantability or fitness for a particular purpose.
# See "LICENSE.GPL" in the source distribution for more information.

# Headers in this file shall remain intact.

from istanbul.main.preferences import Preferences

class RecordingState:
    STOPPED = 0
    RECORDING = 1
    SAVING = 2

class Widget:

    def __init__(self, widget):
        self.widget = widget
        self.name = str(self.widget.get_name())
        # we have 1 gui event and 1 gconf event.
        # so {gui:blah, gconf:blah}
        self.events = {}
        self.preference = None

        self.associate_preference()

    def associate_preference(self):
        preferences = Preferences()
        self.preference = preferences.find_preference(self.name)

class Widgets:
    """
    In the format widget.name:widget
    """
    widgets = {}
