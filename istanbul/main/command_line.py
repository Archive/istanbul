# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4
#
# Istanbul - A desktop recorder
# Copyright (C) 2005 Zaheer Abbas Merali (zaheerabbas at merali dot org)
# Copyright (C) 2006 John N. Laliberte (allanonjl@gentoo.org) (jlaliberte@gmail.com)
# Portions Copyright (C) 2004,2005 Fluendo, S.L. (www.fluendo.com).
# All rights reserved.

# This file may be distributed and/or modified under the terms of
# the GNU General Public License version 2 as published by
# the Free Software Foundation.
# This file is distributed without any warranty; without even the implied
# warranty of merchantability or fitness for a particular purpose.
# See "LICENSE.GPL" in the source distribution for more information.

# Headers in this file shall remain intact.
import optparse

def check_coords(option, opt_str, value, parser):
    t = value.split(',')
    if len(t) != 4:
        raise optparse.OptionValueError('Co-ordinates must have 4 elements (x1,y1,x2,y2)')
    try:
        t = [int(x) for x in t]
    except ValueError:
        raise optparse.OptionValueError('Co-ordinates must be integers')
    parser.values.coords = t


class CommandLine:

    def __init__(self):
        self.parser = optparse.OptionParser()
        self._setup_options()

        self.options = self.parser.parse_args()[0]

    def _setup_options(self):
        self.parser.add_option('-r', '--record',
                          dest="record_file",
                          help="start immediate recording to file")
        self.parser.add_option('', '--coords',
                          action="callback",
                          help="rectangle to register (x1,y1,x2,y2)",
                          type="string",
                          callback = check_coords
                          )

        self.parser.add_option('', '--video-framerate', dest="video_framerate")

        self.parser.add_option('', '--video-size', dest="video_size",
                          help="video size (one of full (default), half, quarter)")

        self.parser.add_option('', '--record-3d',
                          dest="record_3d", action="store_true")
        self.parser.add_option('', '--no-record-3d',
                          dest="record_3d", action="store_false")

        self.parser.add_option('', '--record-mousepointer',
                          dest="record_mousepointer", action="store_true")
        self.parser.add_option('', '--no-record-mousepointer',
                          dest="record_mousepointer", action="store_false")

        self.parser.add_option('', '--record-sound',
                          dest="record_sound", action="store_true")
        self.parser.add_option('', '--no-record-sound',
                          dest="record_sound", action="store_false")

        self.parser.add_option('', '--version',
                          action="store_true", dest="version",
                          default=False,
                          help="show version information")

    def _get_options(self):
        return self.options

    Options = property(_get_options)
