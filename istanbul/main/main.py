# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4
#
# Istanbul - A desktop recorder
# Copyright (C) 2005 Zaheer Abbas Merali (zaheerabbas at merali dot org)
# Portions Copyright (C) 2004,2005 Fluendo, S.L. (www.fluendo.com).
# All rights reserved.

# This file may be distributed and/or modified under the terms of
# the GNU General Public License version 2 as published by
# the Free Software Foundation.
# This file is distributed without any warranty; without even the implied
# warranty of merchantability or fitness for a particular purpose.
# See "LICENSE.GPL" in the source distribution for more information.

# Headers in this file shall remain intact.
try:
    import gconf
except:
    GCONF_AVAILABLE = False
else:
    GCONF_AVAILABLE = True

import optparse
import sys
import os
import gettext
import tempfile
import signal

from istanbul.configure import config
from istanbul.main.save_window import SaveWindow
from istanbul.main.area_select import GtkAreaSelector
from istanbul.main.window_select import WindowSelector
from istanbul.main.preferences import Preferences
from istanbul.main.gconf_client import GConfClient
from istanbul.main.tray_icon import TrayIcon
from istanbul.main.constants import RecordingState
from istanbul.main.screencast import CmdlineScreencast

import pygtk
pygtk.require('2.0')
import gobject
import gtk

# import 0.10 as required
import pygst
pygst.require('0.10')

import gst

import egg.trayicon

_ = gettext.gettext

class Istanbul:
    def __init__(self, options):

        self.encode_later = False
        self.selector = None

        self.options = options
        if self.options.record_file:
            self.current_screencast = CmdlineScreencast(self.stop_callback, options)
            self.current_screencast.start_recording(options)
            signal.signal(signal.SIGINT, self.signal_handler)
            signal.signal(signal.SIGTERM, self.signal_handler)
            print 'istanbul pid:', os.getpid()
        else:
            TrayIcon()

    def stop_callback(self, options):
        print 'stop callback'

    def signal_handler(self, signum, frame):
        print 'signal handler'
        self.current_screencast.stop_recording()

def error_dialog(message):
    dialog = gtk.MessageDialog(type=gtk.MESSAGE_ERROR,
                               buttons=gtk.BUTTONS_OK)
    dialog.set_markup("<b>"+message+"</b>")
    dialog.run()
    sys.exit()

import command_line
def main(args):

    options = command_line.CommandLine().Options

    if options.version:
        print config.version
        return 0

    # check plugins installed
    reg = gst.registry_get_default()
    if not reg.find_plugin("istximagesrc"):
        error_dialog(_("You do not have Istanbul properly installed, cannot find the istximagesrc GStreamer plugin installed."))
    if not reg.find_plugin("theora"):
        error_dialog(_("You do not have the theora GStreamer plugin installed."))
    if not reg.find_plugin("ogg"):
        error_dialog(_("You do not have the ogg GStreamer plugin installed."))

    deskrec = Istanbul(options)
    gtk.main()

if __name__ == "__main__":
    import sys
    main(sys.argv)
