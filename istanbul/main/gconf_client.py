# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4
#
# Istanbul - A desktop recorder
# Copyright (C) 2005 Zaheer Abbas Merali (zaheerabbas at merali dot org)
# Copyright (C) 2006 John N. Laliberte (allanonjl@gentoo.org) (jlaliberte@gmail.com)
# Portions Copyright (C) 2004,2005 Fluendo, S.L. (www.fluendo.com).
# All rights reserved.

# This file may be distributed and/or modified under the terms of
# the GNU General Public License version 2 as published by
# the Free Software Foundation.
# This file is distributed without any warranty; without even the implied
# warranty of merchantability or fitness for a particular purpose.
# See "LICENSE.GPL" in the source distribution for more information.

# Headers in this file shall remain intact.

import gconf

DEBUG=False
class GConfClient(object):

    def __init__(self, directory):
        self.client = gconf.client_get_default()
        #if not self.client.dir_exists(directory):
        self.client.add_dir (directory, gconf.CLIENT_PRELOAD_NONE)
        self.directory = directory + "/"

    def get_entry(self, key, type_):
        """
        returns the value of an entry
        key = dir/key
        type_ = string, boolean, integer or float
        """
        k = self.directory + key
        if DEBUG: print "Setting key: " + k

        if type_ == "string": #FIXME: find a way to automatically set the type of the key?
            s = self.client.get_string(k)
        elif type_ == "boolean":
            s = self.client.get_bool(k)
        elif type_ == "integer":
            s = self.client.get_int(k)
        elif type_ == "float":
            s = self.client.get_float(k)

        return s

    def set_entry(self, key, entry, type_):
        """
        set a value in a key
        key = dir/key
        entry = value of the key
        type_ = string, boolean, integer or float
        """
        k = self.directory + key
        if DEBUG: print "Setting key: " + k

        if type_ == "boolean": #FIXME: find a way to automatically set the type of the key?
            self.client.set_bool(k, entry)
        elif type_ == "string":
            self.client.set_string(k, entry)
        elif type_ == "integer":
            self.client.set_int(k, entry)
        elif type_ == "float":
            self.client.set_float(k, entry)

    def unset_entry(self,key):
        """
        unset (remove) a key
        key = /dir/key
        """
        self.client.unset(self.directory + key)

class IstanbulGConf(GConfClient):
    def __init__(self):
        GConfClient.__init__(self, '/apps/istanbul')

