# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4
#
# Istanbul - A desktop recorder
# Copyright (C) 2005 Zaheer Abbas Merali (zaheerabbas at merali dot org)
# Copyright (C) 2006 John N. Laliberte (allanonjl@gentoo.org) (jlaliberte@gmail.com)
# Portions Copyright (C) 2004,2005 Fluendo, S.L. (www.fluendo.com).
# All rights reserved.

# This file may be distributed and/or modified under the terms of
# the GNU General Public License version 2 as published by
# the Free Software Foundation.
# This file is distributed without any warranty; without even the implied
# warranty of merchantability or fitness for a particular purpose.
# See "LICENSE.GPL" in the source distribution for more information.

# Headers in this file shall remain intact.

import gtk, gobject

class GtkCheckRadio(gtk.MenuItem):
# VBOX
    """
    This class handles having an arbitrary number of check radio buttons, but have it act as
    a real radio control.

    We want to be able to have our elements notify us of an event ( by making GtkCheckRadio emit
    a toggled signal )
    """

    __gsignals__ = { 'toggled': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE,
                                (gobject.TYPE_INT,))
                    }

    def __init__(self, *args):
        """
        Takes a list of elements to create, along with name identifiers.
        {display_name:name}
        """
        self.__gobject_init__()
        gtk.MenuItem.__init__(self)

        self.elements = []
        vbox = gtk.VBox()
        for option in args[0]:
            new_item = gtk.CheckMenuItem(option)
            new_item.set_name = args[0][option]
            new_item.set_draw_as_radio(True)
            new_item.connect("toggled", self.internal_cb)

            self.elements.append(new_item)
            vbox.pack_start(new_item)

        self.add(vbox)

    def internal_cb(self, widget):
        # let subscribers know one of our internal widgets has changed by toggling.
        self.emit('toggled', widget)
