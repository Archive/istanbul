AC_INIT(istanbul, 0.2.2)
AS_VERSION(istanbul, ISTANBUL, 0, 2, 2, 1)
AC_SUBST(RELEASE, $ISTANBUL_RELEASE)

AC_SUBST(PYGTK_REQ, 2.6.3)
AC_SUBST(PYGST_REQ, 0.10.0)
AC_SUBST(GST_REQ, 0.10.0)
AC_SUBST(GSTPB_REQ, 0.10.0)
GST_MAJORMINOR=0.10

AM_INIT_AUTOMAKE($PACKAGE, $VERSION)
AC_SUBST(ACLOCAL_AMFLAGS, "-I m4")

AS_AC_EXPAND(LIBDIR, $libdir)
AC_MSG_NOTICE(Storing library files in $LIBDIR)

AS_AC_EXPAND(DATADIR, $datadir)
AC_MSG_NOTICE(Storing data files in $DATADIR)

AS_AC_EXPAND(SYSCONFDIR, $sysconfdir)
AC_MSG_NOTICE(Storing configuration files in $SYSCONFDIR)

AS_AC_EXPAND(LOCALSTATEDIR, $localstatedir)
AC_MSG_NOTICE(Using localstatedir $LOCALSTATEDIR)

AC_DISABLE_STATIC
AC_PROG_LIBTOOL

dnl check for python
AS_PATH_PYTHON(2.3)

AM_CHECK_PYTHON_HEADERS(HAVE_PYTHON_H=yes, HAVE_PYTHON_H=no)

AS_AC_EXPAND(PYTHONDIR, $pythondir)
AC_MSG_NOTICE(Using pythondir $PYTHONDIR)

AC_PROG_INTLTOOL([0.35.0])
GETTEXT_PACKAGE=istanbul
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE", [The gettext package name])
AM_GLIB_GNU_GETTEXT

dnl check for GStreamer
PKG_CHECK_MODULES(GST, gstreamer-0.10 >= $GST_REQ,
    [GST_SUPPORTED=yes], [AC_MSG_RESULT([$GST_PKG_ERRORS]); GST_SUPPORTED=no])

if test $GST_SUPPORTED = "no"; then
    AC_MSG_ERROR([No GStreamer development packages found.
Please install GStreamer development packages for version 0.10.  Also, check your PKG_CONFIG_PATH.])
fi
AC_SUBST(GST_CFLAGS)
AC_SUBST(GST_LIBS)

dnl check for pygtk
PKG_CHECK_MODULES(PYGTK, pygtk-2.0 >= $PYGTK_REQ)
PYGTK_DIR="`$PKG_CONFIG --variable=pyexecdir pygtk-2.0`"
AC_SUBST(PYGTK_DIR)
AC_MSG_NOTICE(Using pygtk installed in $PYGTK_DIR)
PYGTK_VERSION="`$PKG_CONFIG --modversion pygtk-2.0`"
if test "x$PYGTK_VERSION" = "x2.5.2"
then
  AC_MSG_ERROR([PyGTK 2.5.2 contains known bugs, please install other version])
fi

export PYTHONPATH=$PYGTK_DIR:$PYTHONPATH

PKG_CHECK_MODULES(GTK, gtk+-2.0, HAVE_GTK=yes, HAVE_GTK=no)

dnl checks for gst-python
PKG_CHECK_MODULES(PYGST, gst-python-0.10 >= $PYGST_REQ,
      [PYGST_DIR="`$PKG_CONFIG --variable=pyexecdir gst-python-0.10`"
       AC_SUBST(PYGST_DIR)
       AC_MSG_NOTICE(Using gstreamer-python 0.10 installed in $PYGST_DIR)],
      [AC_MSG_RESULT([$PYGST_PKG_ERRORS])])

saved_PYTHONPATH=$PYTHONPATH
export PYTHONPATH=$PYGST_DIR:$PYTHONPATH
AS_PYTHON_IMPORT([gst],,
    [AC_MSG_ERROR([Unable to import gst-python 0.10 -- check your PYTHONPATH?])
    GST_SUPPORTED=no],
    [import gobject; import pygst; pygst.require('0.10')],
    [assert gst.pygst_version[[1]] == 10 or (gst.pygst_version[[1]] == 9 and gst.pygst_version[[2]] >= 7)])

AC_SUBST(GST_SUPPORTED)

dnl check for egg.trayicon from gnome-python-extras
PKG_CHECK_MODULES(GNOME_PYTHON_EXTRAS, gnome-python-extras-2.0 >= 2.11.3,
    HAVE_EGGTRAYICON=yes, HAVE_EGGTRAYICON=no)
if test "x$HAVE_EGGTRAYICON" = "xno"; then
  AC_MSG_ERROR(gnome-python-extras not found, please install >= 2.11.3)
fi
AS_PYTHON_IMPORT([gconf],,
    [AC_MSG_ERROR([Unable to import gconf -- check your PYTHONPATH?])
    GCONF_SUPPORTED=no], [], [])
AS_PYTHON_IMPORT([egg.trayicon],,
    [AC_MSG_ERROR([Unable to import egg.trayicon -- check your PYTHONPATH?])
    EGG_SUPPORTED=no], [], [])
AS_PYTHON_IMPORT([Xlib.X],,
    [AC_MSG_ERROR([Unable to import python-xlib -- check your PYTHONPATH?])
    XLIB_SUPPORTED=no], [], [])

dnl To build our gst plugins
AC_PROG_CC
PKG_CHECK_MODULES(GST_BASE, gstreamer-base-$GST_MAJORMINOR >= $GST_REQ,
                  HAVE_GST_BASE=yes, HAVE_GST_BASE=no)
dnl We need the base libraries
if test "x$HAVE_GST_BASE" = "xno"; then
  AC_MSG_ERROR(no GStreamer base class libraries found (gstreamer-base-$GST_MAJORMINOR))
fi
dnl If we need them, we can also use the gstreamer-plugins-base libraries
PKG_CHECK_MODULES(GSTPB_BASE,
                  gstreamer-plugins-base-$GST_MAJORMINOR >= $GSTPB_REQ,
                  HAVE_GSTPB_BASE=yes, HAVE_GSTPB_BASE=no)
dnl make _CFLAGS and _LIBS available
AC_SUBST(GST_BASE_CFLAGS)
AC_SUBST(GST_BASE_LIBS)
AC_SUBST(GSTPB_BASE_CFLAGS)
AC_SUBST(GSTPB_BASE_LIBS)

dnl Check for X11
GST_CHECK_FEATURE(X, [X libraries and plugins],
                  [istximagesrc], [
  AC_PATH_XTRA

  dnl now try to find the HEADER
  ac_cflags_save="$CFLAGS"
  ac_cppflags_save="$CPPFLAGS"
  CFLAGS="$CFLAGS $X_CFLAGS"
  CPPFLAGS="$CPPFLAGS $X_CFLAGS"
  AC_CHECK_HEADER(X11/Xlib.h, HAVE_X="yes", HAVE_X="no")

  if test "x$HAVE_X" = "xno"
  then
    AC_MSG_ERROR([cannot find X11 development files])
  else
    dnl this is much more than we want
    X_LIBS="$X_LIBS $X_PRE_LIBS $X_EXTRA_LIBS"
    dnl AC_PATH_XTRA only defines the path needed to find the X libs,
    dnl it does not add the libs; therefore we add them here
    X_LIBS="$X_LIBS -lX11"
    AC_SUBST(X_CFLAGS)
    AC_SUBST(X_LIBS)

    dnl check for Xfixes
    PKG_CHECK_MODULES(XFIXES, xfixes, HAVE_XFIXES="yes", HAVE_XFIXES="no")
    if test "x$HAVE_XFIXES" = "xyes"
    then
        XFIXES_CFLAGS="-DHAVE_XFIXES $XFIXES_CFLAGS"
    fi
    AC_SUBST(XFIXES_LIBS)
    AC_SUBST(XFIXES_CFLAGS)

    dnl check for Xdamage
    PKG_CHECK_MODULES(XDAMAGE, xdamage, HAVE_XDAMAGE="yes", HAVE_XDAMAGE="no")
    if test "x$HAVE_XDAMAGE" = "xyes"
    then
        XDAMAGE_CFLAGS="-DHAVE_XDAMAGE $XDAMAGE_CFLAGS"
    fi
    AC_SUBST(XDAMAGE_LIBS)
    AC_SUBST(XDAMAGE_CFLAGS)
  fi
  AC_SUBST(HAVE_X)
  CFLAGS="$ac_cflags_save"
  CPPFLAGS="$ac_cppflags_save"
])

translit(dnm, m, l) AM_CONDITIONAL(USE_XSHM, true)
GST_CHECK_FEATURE(XSHM, [X Shared Memory extension], , [
  if test x$HAVE_X = xyes; then
    AC_CHECK_LIB(Xext, XShmAttach,
                 HAVE_XSHM="yes", HAVE_XSHM="no",
                 $X_LIBS)
    if test "x$HAVE_XSHM" = "xyes"; then
      XSHM_LIBS="-lXext"
    else
      dnl On AIX, it is in XextSam instead, but we still need -lXext
      AC_CHECK_LIB(XextSam, XShmAttach,
                   HAVE_XSHM="yes", HAVE_XSHM="no",
                   $X_LIBS)
      if test "x$HAVE_XSHM" = "xyes"; then
        XSHM_LIBS="-lXext -lXextSam"
      fi
    fi
  fi
], ,[
  AC_SUBST(HAVE_XSHM)
  AC_SUBST(XSHM_LIBS)
])

if test "x${prefix}" = "x$HOME"; then
  plugindir="$HOME/.gstreamer-$GST_MAJORMINOR/plugins"
else
  plugindir="\$(libdir)/gstreamer-$GST_MAJORMINOR"
fi
AC_SUBST(plugindir)

GST_PLUGIN_LDFLAGS="-module -avoid-version -export-symbols-regex '[_]*(gst_|Gst|GST_).*'"
AC_SUBST(GST_PLUGIN_LDFLAGS)


dnl should we install schemas ?
dnl check for gconftool-2
dnl this macro defines an am conditional, so it needs to be run always
AM_GCONF_SOURCE_2
translit(dnm, m, l) AM_CONDITIONAL(USE_GCONFTOOL, true)
GST_CHECK_FEATURE(GCONFTOOL, [GConf schemas], , [
  AC_PATH_PROG(GCONFTOOL, gconftool-2, no)
  if test x$GCONFTOOL = xno; then
    AC_MSG_WARN(Not installing GConf schemas)
    HAVE_GCONFTOOL="no"
  else
    HAVE_GCONFTOOL="yes"
  fi
  AC_SUBST(HAVE_GCONFTOOL)
])

dnl output stuff
AC_OUTPUT(
Makefile
bin/istanbul
bin/Makefile
common/Makefile
gst/Makefile
istanbul/Makefile
istanbul/configure/config.py
istanbul/configure/Makefile
istanbul/main/Makefile
data/Makefile
doc/Makefile
doc/man/Makefile
po/Makefile.in
)
